# Frontend Mentor - Intro component with sign up form solution

This is a solution to the [Intro component with sign up form challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/intro-component-with-signup-form-5cf91bd49edda32581d28fd1). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Receive an error message when the `form` is submitted if:
  - Any `input` field is empty. The message for this error should say *"[Field Name] cannot be empty"*
  - The email address is not formatted correctly (i.e. a correct email address should have this structure: `name@host.tld`). The message for this error should say *"Looks like this is not an email"*

### Screenshot

![Mobile](./screenshots/mobile.jpg)
![Desktop](./screenshots/desktop.jpg)

### Links

- Solution URL: [Gitlab Repositoy](https://gitlab.com/frontend-mentor-challenge/intro-component-with-signup-form)
- Live Site URL: [Live](https://intro-component-with-signup-form.onrender.com)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - CSS Reset
- [Google Fonts](https://fonts.google.com/)
- [SASS](https://sass-lang.com/) - SASS
- [Gulp](https://gulpjs.com/) - Gulp

### What I learned

### Continued development

### Useful resources

- [Figma](https://figma.com) - For design

## Author

- Website - [Issac Leyva](In construction)
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments