app()

function app() {

    const d = document
    const form  = d.querySelector('.form')
    const inputName = d.querySelector('input[name="first-name"]')
    const inputLast = d.querySelector('input[name="last-name"]')
    const inputEmail = d.querySelector('input[name="email-address"]')
    const inputPassword = d.querySelector('input[name="password"]')
    const validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/

    const input = d.querySelectorAll('input')

    if (form) {

        form.addEventListener('submit', function (e) {
            e.preventDefault()

            if (inputName.value === '') {
                inputName.classList.add('form__input--error')
                inputName.nextElementSibling.classList.add('form__input-icon--error')
                inputName.nextElementSibling.nextElementSibling.innerHTML = 'First Name cannot be empty'
            }

            if (inputLast.value === '') {
                inputLast.classList.add('form__input--error')
                inputLast.nextElementSibling.classList.add('form__input-icon--error')
                inputLast.nextElementSibling.nextElementSibling.innerHTML = 'Last Name cannot be empty'
            }

            if (inputEmail.value === '') {
                inputEmail.classList.add('form__input--error')
                inputEmail.nextElementSibling.classList.add('form__input-icon--error')
                inputEmail.nextElementSibling.nextElementSibling.innerHTML = 'Email cannot be empty'
            }

            if (!validEmail.test(inputEmail.value)){
                inputEmail.classList.add('form__input--error')
                inputEmail.nextElementSibling.classList.add('form__input-icon--error')
                inputEmail.nextElementSibling.nextElementSibling.innerHTML = 'Looks like this is not an email'
            }

            if (inputPassword.value === '') {
                inputPassword.classList.add('form__input--error')
                inputPassword.nextElementSibling.classList.add('form__input-icon--error')
                inputPassword.nextElementSibling.nextElementSibling.innerHTML = 'Password cannot be empty'
            }

            setTimeout(() => {
                input.forEach(input => {
                    input.classList.remove('form__input--error')
                    input.nextElementSibling.classList.remove('form__input-icon--error')
                    input.nextElementSibling.nextElementSibling.innerHTML = ''
                });
            }, 3000);
        })
    }

}